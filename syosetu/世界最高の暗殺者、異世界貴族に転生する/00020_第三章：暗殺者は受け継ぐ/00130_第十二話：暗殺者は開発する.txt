從瑪哈那裡收到了想要的東西，回到了托瓦哈迪。

然後，花了幾天時間在工作室裡調劑藥物。
這種藥幾乎就是毒品，既有依賴性對身體也有壞影響。

但是，如果只是短時間使用的話，可以得到超人的集中力。
如此一來，會讓世界整個看起來變得緩慢。
曾經，在我的世界裡，以運動員們在興奮劑上使用的東西為原型。

核心的素材連魔力都作為營養劑養育著，完成比我的世界裡還要凶惡的東西。

「我不太想依賴這種東西啊。」

是暫時的力量，只是一時的。
而且還會傷害自己的身體。
但是，如果不是那樣的話，也有無法到達的領域。

瑪哈拿到的蘑菇不但被異國部落用來降神，還給人一種驚人的陶醉感。
為使其藥效達到極限，加工，加入了幾種材料。
然後把製成的液體放入注射器中。
因為經口攝取效果不佳，所以要這樣做。

「這是我第一次用這個，應該沒問題吧。」

使用托瓦哈迪地牢裡的死刑犯們，已經確認安全性。
把注射器壓在脖子上，注入藥液。
頭腦變得格外冷清，視野逐漸擴大，不久周圍的動作也變得緩慢了。

愉快的世界。
甚至感到全能感。

試著使用一個【跟隨我的騎士們】獲得的技能之一。
Ｓ等級技能【多重詠唱】

對於人類來說，是不可能同時詠唱多種魔法的魔法。
非常有用，但在Ｓ等級上卻很樸素。
但是，現在可以更深刻地理解那個技能了。
不是單純的使用，而是通過理解和深入讓其進化。

結果⋯⋯【多重詠唱】展現了新的一面。能夠以高速進行不發出聲音的詠唱。

沒錯，【多重詠唱】並不是兩個嘴巴，而是用魔力進行疑似性的詠唱。因為是進行詠唱的疑似體，與普通的詠唱不同，不會被發音的界限速度所束縛，而是被加速。

這樣一來，【高速詠唱】就變成了可能。
與多重詠唱相比，【高速詠唱】有用的機會也很多。

「這就是技能的深奧。」

【多重詠唱】的深處隱藏著這樣的東西。

其他技能也有隱藏的一面。
一想到那些就興奮起來。

而且，現在最感興趣的還是⋯⋯⋯

「【跟隨我的騎士們】。塔爾特和迪雅的強化是最優先的。」

塔爾特和迪雅很強。
甚至不亞於這個國家的騎士。

但是，對於完全超越了人界限的對手來說是無能為力的。
需要提高基礎規格。
為此，我想使用【跟隨我的騎士們】
「⋯⋯啊，藥效斷了嗎？今天就到這裡為止吧。」

噁心和倦怠感。身體像鉛一樣沉。
看來，注射的藥效斷了。
看錶。效果時間是１３分２０秒。這個作為一個目標記住吧。在不得不使用這個的極限戰鬥中，出其不意的失效會導致死亡。

「藥的副作用產生的倦怠感不到一分鐘就消失了，但是不能連續使用。」

由於【超回復】的效果，身體恢復了。
但是，雖說倦怠感很快就會消失，但是【超回復】終究是恢復力的強化，對於不管花多少時間都無法治癒的東西來說是無能為力的。

不能成為另一種副作用──依賴症的對策。
藥物的依賴症，是強烈的快樂在腦中被烙印而引起的，沒有辦法治癒。
讓大腦擺脫快樂的唯一方法是花一些時間。
依賴這種藥的話就無法脫身了。
再加上，如果連續使用的可耐受性，藥效會減弱，效果時間減少。
需要慎重的運用。

「一天之內可以用到的是一瓶嗎？」

如果可以，使用一次的話，我想間隔三天時間。
想馬上進行其他的技能實驗，但是要忍耐一下。

不然，轉眼間我的人格就會壞掉吧。
從這個意義上來說，這傢伙可是王牌。

注意不要輕易使用。
那麼，試探到了技能的深奧。

那麼，接下來就去做那個吧。


◇

為了不產生依賴症，兩天後，把塔爾特和迪雅叫到了院子裡。

下次使用藥物時要嘗試的技能早就決定了，為此。

「今天，給予兩人力量。從勇者得到的技能。使用【跟隨我的騎士們】。這是一個將自己的能力和技能借給對方的力量。」
「那個，借的意思是羅格大人會變弱嗎？」
「不，不是那樣的。是複製。我仍然很強大。所以不用擔心。」

正因為如此，Ｓ等級技能。
每億人中只有一個人擁有，就會被稱為英雄。

「有點期待呢。羅格、埃波納什麼的，已經完全不再是人類了，到現在為止完全沒有追上的感覺。」
「是的，雖然努力想追上，但是差太多了，太遠了⋯⋯但是，只要有這個，就能成為羅格大人的力量！」

兩人都滿懷期待的眼神看著我。

「只是，這個技能有缺點，輸掉賭上重要的東西的戰鬥，違抗我的命令的話，就會失去騎士的資格，技能就無效了。⋯⋯所以，你要做好心理準備。以後你們就不能輸了。」
「嗯，前半段沒有自信」
「最好的是，不做那樣的戰鬥，但是不可避免的時候還是不可避免的。到時候只能下定決心了。」
「不用擔心。做正業的時候沒有問題。因為是暗殺，所以不會成為戰鬥。」
「確實。」

我們不是騎士而是暗殺者。沒有讓人意識到它的存在，只想殺掉它。
在戰鬥發生的時候，已經失敗了一半。

「倒不如說，我更在意後半段。不能違抗羅格的命令。會受到色情的命令嗎？羅格也是男孩子呢。」
「哈哈，這樣啊，雖然有點高興，但是做好心裡準備了。」
「我不會這麼做的。」

即使沒有命令，只要拜託就行了。
有被那樣愛著的自覺。

「哦，差不多該用了，這個技能」
「那個，再變強一點不行嗎？只要不輸給任何人的話，我就放心了。」
「說那種話，不管過了多久，那個時候都不會來。放心吧，塔爾特已經足夠強大了。」

塔爾特沒有多大的才能。
坦率，努力，把許多東西積累起來變強了。
我為那件事感到自豪。

「我怎麼樣呢？」
「迪雅也是，魔法戰的話基本上是無敵的。我保證。」

迪雅以上的魔法使，我不知道。
純粹的技術比我還強。

「那我可以拜託你嗎？因為眼睛是從塔爾特開始的。」
「我知道了。讓我們使用力量吧。」

用注射器將前幾天的藥液打入脖子。
我給這個藥取了這個名字。迪安凱特。

藥物循環，頭腦變得冷清。

「【跟隨我的騎士們】」

發動了技能。

本來，這個技能首先給予力量，其次隨機給予技能。
但是，現在的我。
深入到技能深處的我，可以做些不同的事情。

明白，可以識別。

從我流入到迪雅的東西。
如果在認識了流動的現在，能操縱那個流動。

向著希望的流動，渡過想交給迪雅的東西。
從我的技能和從埃波納那裡得到的技能，從中選擇合適的。

當那些技能和迪雅的才能成為一體的時候，迪雅將作為這個世界上無與倫比的魔法師而覺醒。

想像著那個樣子，我持續釋放著力量。