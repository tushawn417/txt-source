# novel

- title: 進化の実～知らないうちに勝ち組人生～
- author: 美紅
- source: http://ncode.syosetu.com/n4054by/
- title_zh: 進化果實～不知不覺踏上勝利的人生～
- cover: https://i.pinimg.com/564x/22/04/c8/2204c8f8814068bc75d7def5821be27e.jpg
- publisher: syosetu
- date: 2019/08/23 T19:56:00+08:00
- status: 連載

## series

- name: 進化の実～知らないうちに勝ち組人生～

## preface

高中生, 柊誠一, 又胖又醜還有強烈體臭, 因此成為全班霸凌對象
在被自稱是神的聲音傳送到異世界時, 全班也沒有人願意與他組隊
因此只有他一人被送到充滿魔物的森林
靠著神給的技能, 完全解體
以及自聰明猴那邊搶來的進化果實
柊誠一, 朝著非人之路邁進

## tags

- node-novel
- R15
- syosetu
- 残酷な描写あり
- 異世界転移
- 冒険
- 異世界
- 主人公最強
- ハーレム
- ステータス
- スキル
- 魔法
- レベル
- 恋愛
- 成り上がり
- ギャグ 

# contribute

- 咸菜白萝卜
- 阿尔塞拉
- 囧怀表囧
- 纤罂
- 氷姫様
- 大盤陣舞
- goldranx
- 黃昏的那天
- mark26248
- 魅影杀手cc
- (翻譯者名單可能有誤或缺漏, 待查)


# options

## syosetu

- txtdownload_id:

## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou.dip.jp/search.php?text=n4054by&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [进化之实踏上胜利的人生吧](https://tieba.baidu.com/f?kw=%E8%BF%9B%E5%8C%96%E4%B9%8B%E5%AE%9E%E8%B8%8F%E4%B8%8A%E8%83%9C%E5%88%A9%E7%9A%84%E4%BA%BA%E7%94%9F "")
- 



