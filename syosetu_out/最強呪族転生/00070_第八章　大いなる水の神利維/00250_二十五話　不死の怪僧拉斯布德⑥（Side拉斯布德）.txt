雅爾塔米亞浮在半空，一邊制御小亞貝魯球，一邊看著地板的大洞下面，壓碎了拉斯布德的幻之銅立方體和三角錐。

趁其不備的攻擊成功地一時破壊了拉斯布德的身體，但是在半不死身的拉斯布德面前，雅爾塔米亞已經手段用盡。
剩餘的魔力也全部消耗在這個小亞貝魯球裡。
破釜沉舟，這一擊就決出勝負。

立方體微微震動，然後一下子被抬起來。疊在上面的三角錐掉到地上。
立方體被『咒體』染黑全身的拉斯布德單手舉起。

「不管你做什麼，都是木大⋯⋯！誰都無法殺死我！」
「হন 搬運！」

雅爾塔米亞詠唱轉移魔術。
她的身體消失，原地留下魔力之光，直接在拉斯布德的旁邊出現。
她把兩手抱著的巨大的火焰之球推向拉斯布德。

舉著立方體的拉斯布德面對突然使用魔術接近的雅爾塔米亞，一時反應延遲。
他認為雅爾塔米亞會警惕自己『咒體』的超體術和魔術的合擊。

「什、什麼，這個魔術是⋯⋯！」

再加上，雅爾塔米亞抱著的火球，讓拉斯布德即使是跨越過多次死亡也感到極其恐懼。
巨大的球形結界中心，灼熱在蠢動著。
完全無法想像那裡面到底填塞了多少魔力。

「這個角度的話，打到什麼東西也問題不大⋯⋯拉爾克男爵，抱歉了！」

雅爾塔米亞揮動手臂。
然而，小亞貝魯球一動不動。

「啊、啊咧、開玩笑吧！？為什麼啊！？啊啊搞什麼啊！這樣的話，不管三七二十一⋯⋯！」

雅爾塔米亞展開重重魔法陣。
粗略地組成球狀結界，賦予指向性和引爆性能。
球狀結界表面生出坑洞，繼而變得龜裂。

「趁、趁現在！」

與此同時，拉斯布德抓住空隙橫向跳躍。
下一瞬間，小亞貝魯球的球狀結界崩壊，內部的龐大熱量一下子爆發放射。

「嗚，嗚啊啊啊啊啊啊！」

拉斯布德被小亞貝魯球發出的衝擊波彈飛，全身著火在地上翻滾。
他的手被燒焦斷折，握著的咒猿杖的手柄被燒毀。
幼猴的顱骨震動、發出吼叫，尖銳的猿猴悲鳴響起。

放射出的強烈熱光線吹飛了屋邸的北側部分。
不止如此，小亞貝魯球掀開地面，爆散的軌跡延伸出屋邸，把屋邸前數十米的地方變成待開發土地。

「呼哧、呼哧⋯⋯果然，不可以讓結界崩壊麼⋯⋯？」

雅爾塔米亞看著灰飛煙滅、陽光普照的屋邸北側，小聲嘟囔。
她希望沒有發生人員傷害。

「僅僅是擦過就燒起來⋯⋯威力果然太過剩了。亞貝魯那傢伙，到底是什麼想法才製作出這個魔術⋯⋯？正常來說應該要削減⋯⋯嘛，也多虧如此，總算能順利對付拉斯布德⋯⋯」

雅爾塔米亞發出嘆息，煩惱該如何向拉爾克說明。

「雅爾塔米亞小姐！」

聽到梅亞的聲音，雅爾塔米亞轉過頭。
拉斯布德半邊身體被燒糊，他轉過身來，表情憤怒地站著。
他的一隻眼被燒毀，剩下的一隻眼皮也被燒沒了。
直到剛才為止還面無表情的拉斯布德，現在一邊臉露出憎恨和憤怒。

「為、為什麼！？為什麼還能動！？」
「還真敢幹呢，區區一個崩壊成精靈的亡靈！」

『咒體』強化後的拉斯布德反手一拳打在雅爾塔米亞胸部。

「嗚咳！」
「可不僅僅是這樣噢！」

接著，完全不給雅爾塔米亞回復體勢的機會，拉斯布德超高速的體術毆打她的臉、腹部和腰。

「পানি 水喲　দংশন 貫穿！」

連續不斷的暴力擊打，魔術的追加攻擊也沒有落下。
水之刃不停飛出，貫穿破壊雅爾塔米亞的身體。
最後一擊迴旋踢，雅爾塔米亞被猛烈打飛。
她無法採取受身，肩膀撞到地上，翻滾了好幾圈後臉朝上倒著，姿態毫無防備。

「可惡，這個半精靈到底有多耐打⋯⋯！人類的話都已經死了二十回了！」

雅爾塔米亞意識模糊，勉強抬起上身。
拉斯布德腳邊，一部分燒焦了的幼猴顱骨在轉動。

（失策了⋯⋯！沒有直接命中，只是被衝擊波吹飛，沒能破壊掉！）

對策盡出，魔力耗盡的雅爾塔米亞，現在已經沒有辦法對付拉斯布德了。
再加上剛才的連擊，她陷入了身體幾乎完全無法動彈的狀態。
她扭扭勉強抬起來的身體，手撐著地面，但沒有伸手的力氣。

「雖然很想一下把你殺了，但我似乎沒有餘力再折磨不知什麼時候才會死的你。我不得不去修理身體和咒猿杖。橙之魔女，這回就放過你。但下次絶對會把你給殺了，別想著能痛快。利維拉絲藏著多得數不清的邪教和儀式，我將會讓你感受到這個身體受到的所有苦痛⋯⋯！」

說著，拉斯布德把咒猿杖的顱骨撿起來抱住，轉過身去。
他面向的地方是半毀的拉爾克邸的二樓，梅亞所在的房間。

「等、等一下拉斯布德！」
「被逼到這個地步是我預想之外，不過，目的達成了。是我贏了，橙之魔女⋯⋯！呼呼，下次見面的時候，我就是水神大人的四大神官⋯⋯不，我也許會替換掉那個花瓶的小女孩，成為教皇也說不定。就請你好好期待再會之日吧」

拉斯布德的單眼看向雅爾塔米亞。
之後，他很快轉向梅亞，腳踢地面跳起來，侵入拉爾克邸的二樓。

梅亞應戰射出的箭被拉斯布德空手抓住，握碎落在地上。

「呼呼⋯⋯不用害怕，多姆族的小妹妹。只是要稍微跟我走一趟而已，來吧！」

拉斯布德笑著沖向梅亞。

「噫！」

梅亞害怕地閉上眼睛。

───

──這一瞬間，房間裡設置的歐特魯展開了魔法陣。

歐特魯下面一踢地面，跳起來一直線飛向拉斯布德，發出頭部撞擊。

「呣！這是什麼！」

拉斯布德想用單手接住攻擊，但無法抵擋，身體直直受到了歐特魯的頭部撞擊。
攻擊命中的衝擊讓他手上的咒猿杖顱骨飛出，滾在地上。
拉斯布德空出來的兩手抓住歐特魯，想要強行甩開它。

雅爾塔米亞就這樣呆呆地看著，頓了一陣後理解了狀況。

「設置了看守的歐特魯呢⋯⋯」

亞貝魯在動身去制伏納路加魯之前，留下了會以梅亞遭受危險而起動的歐特魯。
梅亞會留在房間裡，也是因為亞貝魯如此囑咐。

（但是，雖然比沒有要好，情況還是很嚴峻⋯⋯）

雅爾塔米亞眯起眼睛，看著歐特魯和拉斯布德互角。

普通的對手的話，一個歐特魯也許就足夠應對。
但是，這個對手很惡劣。
利維拉絲国的殺戮司祭，怪物拉斯布德有『咒體』強化身體，加上究極的體術，即使是在瀕死的狀態，力量也稍稍勝過歐特魯。

「哈啊啊啊啊啊啊啊啊！」

像是要偏轉歐特魯的力量方向，拉斯布德迴轉身體，扭向地板的方向把歐特魯向下砸。
地板被砸出一個漂亮的歐特魯形狀的洞，往下面落下。

因為對手是小型的人形所以可以這樣對付。
拉斯布德氣喘吁吁，為順利地解決而安心下來。

「原來如此⋯⋯這就是亞貝魯的歐特魯麼。要是本人在這裡的話，說不定會有點麻煩呢」

他喘著氣再次看向梅亞。

「來，過來我這裡，赤石的小妹妹⋯⋯」

梅亞周圍出現十個魔法陣。

「呣？」

房間裡放置的十個歐特魯一齊展開了魔法陣。
一瞬間，拉斯布德的思考凝固了。
驚愕與絶望交織，口中漏出「噗噗噗」的聲音。

這跟剛才與歐特魯的搏鬥不一樣，看到這種場面之後，他很容易就想像到自己到底陷入了何等的絶境。
他立刻以單腳為軸迴轉身體，然後以迴轉之勢踏出一步，步幅很大，但體勢卻不凌亂。
他使出極致的體術，毫不含糊地擺出脫離的步法。

然而，別說是逃脫，拉斯布德還沒踏出第二步，後背就被歐特魯的頭部擊中。
當場倒下的拉斯布德被十個歐特魯圍起來，開始無情制裁。
連續的毫不客氣的打擊，他連站起來的機會都沒有。
拉斯布德發出悲鳴。

雅爾塔米亞伏在地上，一副死魚眼看著這個光景。
梅亞從二樓下來，惶恐地靠近雅爾塔米亞。

「沒、沒事嗎，雅爾塔米亞小姐⋯⋯額，雅爾塔小姐」

梅亞想起來改變稱呼。

「⋯⋯喂，那個，是什麼」

確實剛才梅亞有催促過讓雅爾塔米亞別管自己逃走。
那時候還以為她在說什麼胡話，原來是因為這樣。
但即使如此雅爾塔米亞還是覺得無法接受。

「⋯⋯這、這些是亞貝魯走之前設置的保險的歐特魯⋯⋯⋯額，因為是現場決定的事情，他說沒有太大的期待就是了」

梅亞解釋著回答。
其實亞貝魯是說，『萬一來了歐特魯無法對付的傢伙，那就靠雅爾塔米亞了』，但是現在說出來不僅沒有意義，而且會惹人嫌，所以梅亞沒有說。

雅爾塔米亞捂著臉，深深地嘆氣。

（無論如何，沒有出事就再好不過了⋯⋯）

雅爾塔米亞之前還以為拉斯布德說不定可以殺了亞貝魯，但現在看來是她的錯覺。

「啊！」

突然，她想起來拉斯布德的特性。
拉斯布德的不死性來自於咒猿杖。
不管歐特魯怎樣揍拉斯布德，只要那個幼猴的顱骨不被破壊，拉斯布德都無法被打敗。

不知道亞貝魯是怎樣組編歐特魯的停止條件，但如果誤判已經打倒了半不死的拉斯布德，恐怕會讓他逃脫。
必須得回收拉斯布德不死性的根源的那個幼猴的顱骨。

「那個頭蓋骨現在在哪裡⋯⋯！」

雅爾塔米亞抬起頭，看到幼猴的顱骨在二樓的房間裡穩穩地旋轉著。
果然歐特魯無法感知到咒猿杖。
不破壊那個的話，不能讓人放心。

（所幸，我多少回復了一點⋯⋯把那個撿起來回收這種程度的事，我現在也能做到⋯⋯）

雅爾塔米亞站起來，浮遊移動向拉爾克邸的二樓。
警戒著被歐特魯持續毆打著的拉斯布德，她撿起幼猴的頭蓋骨。

「魔、魔女⋯⋯魔女！雅爾塔米亞，雅爾塔米亞閣下！」

拉斯布德發出哀求的聲音。

「我投降！停下、請停下來！」

他像是懇求一樣抬起手腕，但被歐特魯踩壊。

「⋯⋯才不知道呢，又不是我的。額⋯⋯這些是為了將敵人無力化，大概，在你暈過去之前是不會停手的吧？」
「那就摧毀魔杖吧！只要這些東西還在，我是不會失去意識的⋯⋯！嗚！剛才那個魔術再使一次，應該能完全破壊的⋯⋯拜託了！」

雅爾塔米亞看向她撿起來的咒猿杖的顱骨。
確實再一次使出小亞貝魯球的話也許可以把它破壊，但現在她沒有這個餘力。
但本來雅爾塔米亞就沒有義務要解放拉斯布德。
甚至還想叫歐特魯一直揍他，直到讓他屈辱地死掉為止。

「⋯⋯那麼，你就先保持這個狀態一段時間吧。這個就由我保管吧」
「橙之魔女啊啊啊啊啊啊！求求你！」

拉斯布德的咆哮被歐特魯毆打他下巴止住了。

（⋯⋯我之前都在幹啥啊）

雅爾塔米亞心裡充滿了難以言喻的虛無感，最後再一次嘆息起來。