「回家……」
「路，路西法大人……請您保持清醒」

把臉埋在蕾娜豐滿的胸裡，吾史無前例的低落著。

「露米艾爾在等吾……得去買1個什麼土特產給那傢伙吶……買什麼她會高興呢，雖然考慮了很多但什麼都想不到啊，哈哈。蕾娜，你怎麼想？」
「路西法大人，您的說法變含糊了。請別灰心……」

將身體交給乖乖的撫摸吾的頭的蕾娜。就那樣讓她為吾做了膝枕。

(插圖003)

「……這個國家有很多強者和將來有望的人……可已經沒有人認為魔族是威脅了吶」
「500年間沒有大的爭鬥。雖然路西法大人和其他的魔神們或許覺得就像是最近的事一樣，但我想作為人類忘記威脅的時間是足夠了吧」

蕾娜一邊哄吾一邊繼續說。

「關於外交我覺得插嘴是不知趣，所以至今為止都沒有提及，但路西法大人為什麼要放任帝國呢？ 如果是您的話，認真攻克的話這種國家馬上就能掌握了吧？」

「是為了避免和各個國家的糾紛。以吾國北方的聖王國為首，對吾們魔族感到不快的人數很多吶。如果那時想要攻陷帝國的話，即使出現援助的國家也不奇怪」
「我知道這很失禮，但請允許我特意發言。我認為以戴涅布萊的總力，即使將各個國家包含在內也不是對手」

吾稍加沉思。
蕾娜說的話很有道理。實際上在魔族之中也經常出現這種意見。
不如說，如果舉行由包含支配戴涅布萊的吾在內的被稱為王族的魔神們不定期召開的會議的話，大半的時間會是關於這種事情的協商。

「這個即便是在王族中也是意見分歧吶。吾並不想侵略。何況不僅是帝國，吾不打算與其他所有國家為敵。如果那樣做的話不論吾們多強力，都會出現不少犧牲」

吾繼續「而且」這樣說道。

「大陸的一半已經屬於吾們了。你知道前代路西法就已經掌握了戴涅布萊西方的諸國吧？」
「是的，我知道。已經是1500多年前的事了呢」
「唔姆……。因為至今為止沒有被問過所以吾沒說過，但機會正好吾就說說吧。吾滅殺前代的理由」

吾回想起當時的光景。血與肉的狂亂和魔神們異常的好戰的嗜好。
不管是強者還是弱者，只是重複蹂躪、掠奪，成為威脅的人們也包含以儆效尤的用意，全部以極其殘忍的方法殺害。
有1天沒看見血就無法滿足，也有人會把在自己率領的部隊中沒用的人拽出來無意義地殺掉。最甚的例子就是前代路西法。

「那個是想要支配這個大陸全境。而當時的吾們也有達成那個的力量。所以，吾殺了前代」
「那是為什麼呢？ 魔族的繁榮不是可喜的事嗎？」
「那個的目標是大陸的支配，但那終將會變成吾們魔族滅亡的端緒」

感覺蕾娜有些困惑似的。

「有力量的人會被其吞噬。在本能的驅使下傷害他人，只是蹂躪的將來——是失去去處的力量的暴走在等著。失去了狩獵場也依然不衰弱的殺戮的爪牙必定將會朝向同胞」
「……！」
「前代沒有理解這個。所以吾殺了他成為了王。為了不讓魔族走上毀滅的道路，吶」

假如，吾就那樣不和前代的做法唱反調，只是一味持續殺戮的話……吾能斷言今天的戴涅布萊魔族國早就不存在了。
當時的魔族幾乎所有人都渴求血與肉到了如此地步，遇到什麼就將不管做什麼都不會滿足的破壞衝動砸向什麼，僅僅只考慮蹂躪他人的事。
那時的魔族很瘋狂。那無非是超越了魔族特性的狂亂的熱潮。

「您是說霸者終將及至滅亡呢」
「對。吾們魔神有時會變得無法制御過強的力量。當時的王族的七柱全員都是那個狀態……所以，吾不做不行。儘管只是低級魔神之身，力量卻超過前代的吾不做不行」

殺同胞時的事即使是現在也偶爾會想起。
當然，敵人不只是前代。其他王族們也好，魔神們也好，他們的手下們也好……數不盡的切開、燒光，一個勁地殺戮。
無論怎麼說自己也充滿了破壞衝動，這只手都殺死了數量超過常規的同胞。

活下來的只有半死不活的王族們和寥寥無幾的手下們。
儘管是不做不行的事，儘管如此——。
光是回憶起就忍不住想吐。非常不舒服。

「……嚴肅的話說太多了吶。吾累了」
「是，非，非常抱歉！ 我這種人多管閑事了……！」
「無妨。比起那個再多治癒治癒吾。寵寵吾。說乖乖吧」
「好，好的」

和蕾娜一起橫臥在床上，以把身體交給她的形式撒嬌。
……變得不知道就這樣呆在帝國到底是不是有益了。怎麼做才好呢。
嘛，算了。明天的事明天考慮吧。今天已經什麼都不想考慮了。


---


翌日。

接受著上午的魔術講義的授課的我一邊在窗邊的座位上發呆一邊等待著時間經過。
教師教的是魔術的基礎中的基礎。即使是人類的孩子也知道，不值得聽。

業炎術是炎，深水術是水，雷轟術是雷，土隆術是土，風迅術是風。到這裡為止是五大元素魔術的本源。
然後有些特殊的神聖術是聖潔的術，而晦冥術如果用錯的話會通向操縱危險的黑暗的術，全部7系統就是構成基本魔術的術式。雖然當然也有例外。
……因此，事到如今即使被得意洋洋地講那種東西也會讓人傷腦筋。在入學這個學園的人中連這點知識都沒有的，就算是和魔術完全無緣的獸人都不可能。

「……道爾！ 提奧道爾！ 喂，你在聽嗎！」
「嗯，什麼？」

壯年的男性教師叫了我。

「不是什麼！ 關於這個魔術的魔法陣你有什麼想說的嗎！？」

看向黑板的話，上面用粉筆畫著第2位階的魔法陣。系統是雷轟術。是操作雷的魔術。
可是刻在魔法陣中的文字的拼寫是業炎術這個操作炎的術式。這樣子這個魔術是不會成立的。
所以我說道。

「畫這個魔法陣的人不是傻瓜嗎？」
「什麼……！？ 你，你這傢伙……！」

啊啊，是這樣啊。那是這個教師畫的。不由得說出了真心話。

「用這個魔法陣的話術式不會成立喲」
「吼……那麼，理由呢」
「因為雖然是雷轟術式，但刻在魔法陣裡的文字是業炎術式」
「……呋呋」

教師露出了目中無人的笑容。
在這裡我察覺到了。是這樣啊，這個人為了讓我出醜故意出了這種簡單的問題嗎。
如果以普通的回答結束的話，他就打算高聲宣言我是平庸的學生後高興地發表自大的回答吧。

「雖說是特待生名額但終歸是孩子啊！ 聽好，這個術式是」
「你想說是復合術式吧？」
「什！？」

我離開座位走到教師旁。教室中充滿了熙熙攘攘的喧囂。

「用老師畫的現階段的魔法陣術式是不會成立的。就這樣子的話儘管寫了『炎啊，燃盡周圍』魔法陣也還是雷轟術所以沒意義。但是，在這篇文章的頭上這樣附加」

我在被刻在畫在黑板上的魔法陣裡的文字頭上寫上了『由雷而顯現的』。

「通過寫『由雷而顯現的炎啊，燃盡周圍』，首先雷轟術式發動。然後緊接著就那樣接替雷轟術式的魔力，業炎術式發動，就像文章裡一樣會把周圍的東西全部燃燒盡。……不過，靠第2位階程度的魔力沒法產生那麼強力的炎呢。怎麼樣，老師」
「嗚咕……咕努努……！」
「我實際畫一下魔法陣吧。喏」

我打了個響指，學生們面前就浮起了如我所說的魔法陣。全部學生都驚愕了。

「咏唱都不用嗎！？」
「復，復合術式這麼簡單就……」

好像和魔術關係疏遠的人們驚訝特別強。

「啊啊，沒事的喲。不會讓它發動的。喏，這樣浮起魔法陣意味著即使是看起來最初就錯了的術式，通過附加或是改寫裡面的文章，也能發動這種在風裡兼具雷和炎兩者特性的復合術式」

學生們的幾人發出了感嘆之聲。
嘛，如果不熟悉魔術的話連復合術式都腦子轉不過來吧。
對在入學考試沒選擇魔術的學生來說或許很有意思。

「復合術式再多重疊幾重也是做得到的。不僅僅是二重、三重，發動具備加上神聖術和晦冥術的所有7大要素的魔術也是可能的喲」
「那，那種事是不可能的！」

教師反駁道。

「復合術式也有限度。不管如何鍛鍊，三重……就算是極其優秀的人重疊四重也是極限了！ 在那之上頭腦的演算能力跟不上，說到底魔力就不可能夠！」
「嗯，你……不對，老師也許辦不到，但實際能做到。只是重疊多少位階也會上升多少，消費的魔力的量也相差懸殊。不過」
「不只是我，我是說在這裡的所有人那種絕技都是不可能的！！」

姆。或許有一番道理吶，那個。雖然我的話能做到，但現在在這裡的學生中沒有能感到在魔術上突出的人。
如果是像剛才的問題一樣的單純的復合術式，鍛鍊好的話也許做得到但在這之上就苛刻了嗎。
哪怕莉茲或朱利安在也好啊。如果是那2個人的話，有將來做得到的可能性。

魔術是才能就是一切的世界。不管有多少知識量，沒有魔力的話都無計可施。
即使在魔力上突出，人類也有作為種族的限界。縱使有魔力，身體也保持不了魔術的行使呢。
教再怎麼鍛鍊也絕對做不到的事或許是沒意義。這個老師還算正確吧。

「抱歉，是我多管閑事了。那麼我可以回去了嗎？」
「……快回座位去」

我坐到自己的座位上，又發著呆度過了。
無聊啊，我在心中如此嘟囔。
